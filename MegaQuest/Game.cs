﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MegaQuest
{
    internal static class Game
    {

        public static int ActStartRoom(int roomNumber, bool hasItem)          // Сценарий стартовой комнаты (комната № 1)
        {
            Console.Clear();
            Console.WriteLine("Вы видите огромную дверь расписаную многочисленными рунами, однако по всей видимости чего то не хватает");
            Console.WriteLine("Для того чтобы открытия двери не хватает какого магического артефакта");
            Console.WriteLine("Доступные действия");
            Console.WriteLine("1. Выйти в коридор");
            int answer;
            if (hasItem)                                            // Если у игрока имеется квестовый предмет, то появляется опция открыть дверь
            {
                Console.WriteLine("2. Открыть дверь");
                answer = GetIntInRange(2);
            }
            else answer = GetIntInRange(1);

            if (answer == 1)
            {

                roomNumber = 2;
            }
            else if (answer == 2)                                        //Если игрок открыл дверь - пускаем титры
            {
                Console.Clear();
                Story.Outro();

            }
            
            return roomNumber;


        }


        public static int ActHall(int roomNumber) //Сценарий коридора (комната №2)
        {
            Console.Clear();
            Console.WriteLine("Вы попадаете в другую комнату, в ней заметно меньше остатков.");
            Console.WriteLine("Пройдя немного по коридору вы оказываетесь у развилки");
            Console.WriteLine("-Те, кто решил пойти на лево издавали очень страшные крики, поэтому я бы предложил тебе выбрать правую дверь.");
            Console.WriteLine("Вы очень сильно сомневаетесь в словах Чикизябрика,думая что это его ловушка, но выбор всё же сделать нужно, не хочется умереть в этом подземелье от обезвоживания.");
            Console.WriteLine("Доступные действия");
            Console.WriteLine("1. Вернутся назад");
            Console.WriteLine("2. Зайти в дверь направо");
            Console.WriteLine("3. Зайти в дверь на лево");
            int answer = GetIntInRange(3);

            if (answer == 1) roomNumber = 1;
            else if (answer == 2) roomNumber = 4;
            else if (answer == 3) roomNumber = 3;

            return roomNumber;
        }


        public static int ActKeyRoom(int roomNumber) //Сценарий комнаты с ключом (Комната № 3)
        {
            Console.Clear();
            Console.WriteLine("Войдя в комнату вы оказываетесь в какой-то библиотеке.");
            if (Program.Key == false)                                                                  //Если у игрока нет ключа он его заметит
                Console.WriteLine("Немного осмотрев её вам на глаза попадается ключ.");
            Console.WriteLine("Осмотрев библиотеку до конца вы понимаете что дальше дороги нет и остаётся только путь назад и проверить левую комнату на той самой развилке.");
            Console.WriteLine("-Похоже тут больше ничего нет-уверенно сказал ты.");
            Console.WriteLine("-Да, очень похоже на то. Видимо всё же придётся тебе пройти в левую комнату.");
            Console.WriteLine("Доступные действия");

            int answer;
            if (Program.Key == false)
            {

                Console.WriteLine("1. Вернутся назад");
                Console.WriteLine("2. Взять ключ");
                answer = GetIntInRange(2);
            }
            else                                                       //Когда игрок забрал ключ опция взять его второй раз пропадает
            {

                Console.WriteLine("1. Вернутся назад");
                answer = GetIntInRange(1);


            }

            if (answer == 1) roomNumber = 2;


            else if (answer == 2 && Program.Key == false)
            {
                Program.Key = true;
                Console.Clear();

            }

            return roomNumber;
        }


        public static int ActDanger(int roomNumber, bool hasKey) //Сценарий комнаты с монстром (комната №4)
        {
            Console.Clear();
            Console.WriteLine("Попав в эту комнату вы видите огромное страшное существо с головой быка. По всем частям комнаты разбросаны разорванные тела.");
            Console.WriteLine("-Тихо, вроде оно спит-сказал Чикизябрик-Давай попробуем тихонько пробраться к двери в следующую комнату.");
            Console.WriteLine("Он пальцем показывает на такую же огромную дверь, всю исцарапанную этим огромным монстром.");
            Console.WriteLine("Доступные действия");
            Console.WriteLine("1. Вернутся назад");
            Console.WriteLine("2. Пройти в дверь");


            int answer = GetIntInRange(2);
            if (answer == 1) roomNumber = 2;
            else if (answer == 2 && hasKey == true) //Если игрок взял ключ, то дверь открывается и игрок попадает в следущую комнату
            {
                Console.WriteLine("Вы аккуратно пробираетесь вдоль стены и толкнув дверь понимаете что она закрыта.");
                Console.WriteLine("-Что же нам делать?-вскрикнули вы.");
                Console.WriteLine("-Тихо, у тебя же есть ключ, попробуй его, это наш единственный шанс.");
                Console.WriteLine("Действительно, вы уже позабыли об этом ключе увидев это огромное чудище. Аккуратно достав ключ, вы вставляете его в замочную скважину и к вашему счастью он поворачивается.");
                roomNumber = 5;
            }
            

            else if (answer == 2 && hasKey == false)              // Если у игрока нет ключа, то геймовер
            {
                Console.WriteLine("Вы аккуратно пробираетесь вдоль стены и толкнув дверь понимаете что она закрыта.");
                Console.WriteLine("-Что же нам делать?-вскрикнули вы.");
                Console.WriteLine("Толкнув дверь она издала громкий звук, по которому вы поняли что всё же она заперта.");
                Console.WriteLine("-О...о...обернись!-вскрикнул Чикизябрик.");
                Console.WriteLine("Оглянувшись вы только успели увидеть как это огромное чудовище прыгнуло на вас.");

                Story.Gameover();


            }


            return roomNumber;


        }


        public static int ActQuestRoom(int roomNumber) // Сценарий сокровищницы (комната №5)
        {
            Console.Clear();

            if (Program.Item == false)
            {
                Console.WriteLine("На входе вас ослепляет яркий свет, но постепенно ваши глаза привыкают и вы понимаете что оказались в огромной сокровищнице.");
                Console.WriteLine("-ВАУ, СКОЛЬКО ЖЕ ТУТ ЗОЛОТА!-закричали вы.");
                Console.WriteLine("-Вижу я, только присмотрись внимательнее.");
                Console.WriteLine("В недоумении от слов Чикизябрика вы начинаете разглядывать комнату и замечаете останки, лежавшие в золоте.");
                Console.WriteLine("-А почему они мертвы?");
                Console.WriteLine("-Точно не знаю, но я слышал легенду о проклятой сокровищнице, в которой несметные богатства, но самой ценной является рунный камень способный открыть выход из этого места!.");
                Console.WriteLine("Ещё немного посовещавшись вы решаете найти эту чашу.");
                Console.WriteLine("-Кажется я нашёл-выкрикнул Чикизябрик из другого конца сокровищницы.");
                Console.WriteLine("Вы подбежали к нему и действительно там был рунный камень, который выглядит один в один как описывалось в легенде.");
                Console.WriteLine("Вы берёте камет и с вами ничего не произошло.");
                Console.ReadLine();
            }
            else
                Console.WriteLine("На входе вас ослепляет яркий свет, вы  оказались в огромной сокровищнице.");


            int answer;
            if (Program.Item == false)
            {

                Console.WriteLine("1. Вернутся назад");
                Console.WriteLine("2. Взять камень");
                answer = GetIntInRange(2);
            }
            else
            {

                Console.WriteLine("1. Вернутся назад");
                answer = GetIntInRange(1);
            }

            if (answer == 1) roomNumber = 4;
            else if (answer == 2 && Program.Item == false)
            {

                Program.Item = true;
                Console.Clear();
            }

            return roomNumber;

        }


        private static int GetIntInRange(int optionsNum) //Проверка допустимости вводимого значения. Метод принимает кол-во возможных действий игрока и ждёт когда он введёт подходящий ответ
        {
            string input = Console.ReadLine();
            int num = -1;
            bool converted = int.TryParse(input, out num);             // Проверка получилось ли конвертировать входные данные в int32
            bool inRange = num >= 1 && num <= optionsNum;              // Проверка находится ли вводимое значение в диапозоне возможных вариантов ответа

            while (!converted || !inRange)
            {
                Console.WriteLine("НЕВЕРНАЯ ОПЦИЯ!");
                input = Console.ReadLine();
                num = -1;
                converted = int.TryParse(input, out num);
                inRange = num >= 1 && num <= optionsNum;
            }
            return num;

        }


    }
}
