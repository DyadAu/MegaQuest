﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MegaQuest
{


    public class Program
    {



        private static int roomNumber = 1;          //Номер комнаты в которой находится игрок
        private static bool hasKey = false;         //Наличие у игрока ключа
        private static bool hasItem = false;        //Наличие у игрока квестового предмета


        public static bool Key
        { 
            get { return hasKey; } 
            set { hasKey = value; }
        }
        
        public static bool Item
        { 
            get { return hasItem; } 
            
            set {  hasItem = value; } 
        }
            
            
        public static void Main(string[] args)
        {
            Story.Intro(); //Сюжетное введение

            while (true)   //Бесконечный цикл который проверяет в какой комнате находится игрок и какой сценарий запускать
            {
                if (roomNumber == 1) roomNumber = Game.ActStartRoom(roomNumber, hasItem);
                
                    

                else if (roomNumber == 2) roomNumber = Game.ActHall(roomNumber);

                else if (roomNumber == 3) roomNumber = Game.ActKeyRoom(roomNumber);

                else if (roomNumber == 4) roomNumber = Game.ActDanger(roomNumber, hasKey);

                else if (roomNumber == 5) roomNumber = Game.ActQuestRoom(roomNumber);


            }


        }

    }
}


